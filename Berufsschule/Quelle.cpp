#include <iostream>

int main() {
	unsigned short int test = 65535;
	std::cout << test << std::endl;
	test = test++;
	std::cout << test << std::endl;
	test = test - 2;
	std::cout << test << std::endl;
	getchar();
	return 0;
}
