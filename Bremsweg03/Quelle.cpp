#include <iostream>;
#include <cmath>

using namespace std;

int main() {

	// Variable 
	float f_Anhaltweg;
	float f_Geschwindigkeit;
	float aufprallgeschwindigkeit;
	int i_Reaktion;
	int i_Verzoegerung;
	char c_Nochmal;
	int i_Verhaeltnisse;
	float f_Abstand;
	do {
		// Variable z�rucksetzen 
		f_Anhaltweg = 0;
		f_Geschwindigkeit = 0;
		f_Geschwindigkeit = 0;
		i_Reaktion = 0;
		i_Verzoegerung = 0;
		c_Nochmal = ' ';
		i_Verhaeltnisse = 0;
		f_Abstand = 0;
		// Bildschirm l�chen 
		system("cls");

		cout << "-- Bestimmung des Anhaltewegs -- " << endl << endl;
		cout << "Bitte geben Sie die gefahrende Geschwindigkeit in Km/h ein: ";
		cin >> f_Geschwindigkeit;
		cout << endl << "Bitte geben Sie die restliche Strecke bis zum Hindernis in m ein: ";
		cin >> f_Abstand;
		cout << endl << "Bitte geben Sie die Reaktionszeit in s (Schrecksekunde ca.1) ein: ";
		cin >> i_Reaktion;
		do {
			cout << endl << "Bitte geben Sie die den Verzoegerungswert an: " << std::endl;
			std::cout << "1: trockener Asphalt" << std::endl;
			std::cout << "2: nasser Asphalt" << std::endl;
			std::cout << "3: trockene Beton" << std::endl;
			std::cout << "4: nasser Beton" << std::endl;
			cin >> i_Verhaeltnisse;
			switch (i_Verhaeltnisse)
			{
			case 1:
				i_Verzoegerung = 3;
				break;
			case 2:
				i_Verzoegerung = 5;
				break;
			case 3:
				i_Verzoegerung = 7;
				break;
			case 4:
				i_Verzoegerung = 9;
				break;
			default:
				std::cout << "Falsche Eingabe! Bitte erneut eingeben.";
				break;
			}
		} while ((i_Verhaeltnisse >= 4) || (i_Verhaeltnisse <= 0));
		f_Geschwindigkeit = f_Geschwindigkeit / 3.6;
		f_Anhaltweg = (f_Geschwindigkeit * i_Reaktion) + (pow(f_Geschwindigkeit, 2) / (2 * i_Verzoegerung));
		cout << endl << "Restlicher Weg bis zum Hindernis: " << f_Abstand;
		cout << endl << "Benoetigten Anhaltsweg: " << f_Anhaltweg;
		if (f_Anhaltweg < f_Abstand) {
			cout << endl << endl << "Glueck gehabt!";
		}
		else {
			cout << endl << endl << "Es kam zum Crash!";
		}
		cout << endl << endl << "Wollen Sie es noch eine Berechnung durchf�ren? (J/j)";
		cin >> c_Nochmal;

	} while (c_Nochmal == 'j' || c_Nochmal == 'J');


	return 0;
}