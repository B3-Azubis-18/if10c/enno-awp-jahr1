#include<iostream>
#include<Windows.h>

using namespace std;

void SortiereDreiZahlen(int&, int&, int&);
void TauscheZahlen(int&, int&);

int main() {
	SetConsoleOutputCP(CP_UTF8);

	int Z1 = 0;
	int Z2 = 0;
	int Z3 = 0;

	cout << "Bitte geben Sie die erste Zahl ein: ";
	cin >> Z1;
	cout << "Bitte geben Sie die zweite Zahl ein: ";
	cin >> Z2;
	cout << "Bitte geben Sie die dritte Zahl ein: ";
	cin >> Z3;

	SortiereDreiZahlen(Z1, Z2, Z3);

	cout << "Sortiert: " << Z1 << " " << Z2 << " " << Z3 << endl;


	getchar();
	getchar();
	return 0;
}

void SortiereDreiZahlen(int& z1, int& z2, int& z3) {
	if (z2 > z3) {
		TauscheZahlen(z2, z3);
	}

	if (z1 > z2) {
		TauscheZahlen(z1, z2);
		if (z2 > z3) {
			TauscheZahlen(z2, z3);
		}
	}
}

void TauscheZahlen(int& z1, int& z2) {
	int tmp = z1;
	z1 = z2;
	z2 = tmp;
}
