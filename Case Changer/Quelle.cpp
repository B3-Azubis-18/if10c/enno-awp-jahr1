#include <iostream>

int main() {
	char buchstabe;

	//Eingabe
	std::cin >> buchstabe;

	//Verarbeitung
	buchstabe = buchstabe - 32;

	//Ausgabe
	std::cout << "Buchstabe: " << buchstabe;

	getchar();
	getchar();
	return 0;
}