#include <iostream>;

int main() {
	unsigned int hoehe = -1;
	char eingabe = ' ';
	bool again = true;

	std::cout << "- Variante 2 -" << std::endl;
	do {
		std::cout << "Hoehe des Baumes eingeben <5-40>: ";
		std::cin >> hoehe;
		std::cout << std::endl;

		for (int i = 0; i < hoehe; i++) {
			for (int j = 0; j <= i; j++) {
				std::cout << "x";
			}
			std::cout << std::endl;
		}

		do {
			std::cout << std::endl;
			std::cout << "Noch ein Baum? <j/n>: ";
			std::cin >> eingabe;
			if (eingabe == 'j' || eingabe == 'J') {
				break;
			}
			else if (eingabe == 'n' || eingabe == 'N')
			{
				again = false;
				break;
			}
			else
			{
				std::cout << "Falsche Eingabe! Bitte versuche es erneut!";
			}
		} while (true);
	} while (again);

	return 0;
}