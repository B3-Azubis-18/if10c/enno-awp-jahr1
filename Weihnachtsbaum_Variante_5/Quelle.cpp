#include <iostream>
#include "ChristmasTree.h"

int main() {
	int height = -1;
	int width = 20;
	bool again = false;
	char input = ' ';

	std::cout << "- Variante 4 -" << std::endl;
	do {
		std::cout << "Hoehe des Baumes eingeben <5-40>: ";
		std::cin >> height;
		std::cout << std::endl;

		width = height * 2;
		ChristmasTree tree{ height, width };
		tree.buildTree();
		tree.printTree();
		tree.~ChristmasTree();

		do {
			std::cout << std::endl;
			std::cout << "Noch ein Baum? <j/n>: ";
			std::cin >> input;
			if (input == 'j' || input == 'J') {
				break;
			}
			else if (input == 'n' || input == 'N')
			{
				again = false;
				break;
			}
			else
			{
				std::cout << "Falsche Eingabe! Bitte versuche es erneut!";
			}
		} while (true);
	} while (again);
}
