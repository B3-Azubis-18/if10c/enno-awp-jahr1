#include <iostream>;
#include <Windows.h>

int main() {
	SetConsoleOutputCP(CP_UTF8);

	unsigned int till = 0;
	unsigned int sum = 0;

	std::cout << "Bitte gewŁnschte Zahl eingeben: ";
	std::cin >> till;
	for (int i = 0; i <= till; i += 2) {
		std::cout << i << std::endl;
		sum += i;
	}
	std::cout << "Ergebnis: " << sum << std::endl;
	std::getchar();
	std::getchar();

	return 0;
}