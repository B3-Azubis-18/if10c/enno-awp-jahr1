#include <iostream>;
#include <Windows.h>

int main() {
	// FIXME: Overflow of odd if till is even.
	SetConsoleOutputCP(CP_UTF8);

	unsigned int till = 0;
	unsigned int odd = 0;
	unsigned int sum = 0;

	std::cout << "Bitte gewŁnschte Zahl eingeben: ";
	std::cin >> till;
	odd = till;
	if (odd % 2 == 0) {
		odd--;
	}
	for (int i = 0; i <= till; i += 2) {
		std::cout << i << "\t" << odd << std::endl;
		odd -= 2;
		sum += i;
	}
	std::cout << "Ergebnis: " << sum << std::endl;
	std::getchar();
	std::getchar();

	return 0;
}