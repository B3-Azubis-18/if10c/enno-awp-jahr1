#include <iostream>
#include <Windows.h>

int menue();

float eingabeBetrag();

float berechneEndbetrag(float f_betrag, float f_satz, float f_laufzeit);

void berechneEndbetrag_jaehrlich(float anfangskapital, float zinssatz, float laufzeitInJahre);

int main() {
	float kapital = 0.0;
	float endkapital = 0.0;
	float zinssatz = 0.0;
	int laufzeit = 0;
	int auswahl = 0;
	char nochmal = 'n';
	char eingabe;

	SetConsoleOutputCP(CP_UTF8);
	do {
		std::system("cls");
		switch (menue()) {
		case 1:
			std::cout << "Bitte gebe das Kapital ein: ";
			kapital = eingabeBetrag();
			std::cout << "Bitte gebe den Zinssatz ein: ";
			zinssatz = eingabeBetrag();
			std::cout << "Bitte gebe die Laufzeit ein: ";
			laufzeit = eingabeBetrag();
			endkapital = berechneEndbetrag(kapital, zinssatz, laufzeit);
			std::cout << "Das Endkapital ist: " << endkapital << std::endl;
			break;
		case 2:
			std::cout << "Bitte gebe das Kapital ein: ";
			kapital = eingabeBetrag();
			std::cout << "Bitte gebe den Zinssatz ein: ";
			zinssatz = eingabeBetrag();
			std::cout << "Bitte gebe die Laufzeit ein: ";
			laufzeit = eingabeBetrag();
			berechneEndbetrag_jaehrlich(kapital, zinssatz, laufzeit);
			break;
		default:
			std::cout << u8"Dieser Men�punkt ist nicht vorhanden." << std::endl;
		}

		std::cout << u8"Nochmal berechnen? ('j'/'J' f�r nochmal) - ";
		std::cin >> nochmal;

	} while (nochmal == 'j' || nochmal == 'J');

	std::system("pause");
	return 0;
}

int menue() {
	int option;
	std::cout << "______________________________" << std::endl;
	std::cout << u8"Bitte Men�punkt ausw�hlen" << std::endl;
	std::cout << "<1> Sparplan berechnen" << std::endl;
	std::cout << "<2> Sparplan mit j�hrlicher Ausgabe" << std::endl;
	std::cout << "______________________________" << std::endl;
	std::cout << "Ihre Wahl : ";
	std::cin >> option;
	if (option > 0 && option < 3) {
		return option;
	}
	else {
		return -1;
	}
}

float eingabeBetrag() {
	float eingabe = 0.0;
	std::cin >> eingabe;
	if (eingabe < 0.0) {
		return eingabe * -1;
	}
	else
	{
		return eingabe;
	}
}

float berechneEndbetrag(float f_betrag, float f_satz, float f_laufzeit) {
	return f_betrag * pow(1 + f_satz, f_laufzeit);
}

void berechneEndbetrag_jaehrlich(float anfangskapital, float zinssatz, float laufzeitInJahre)
{
	for (int i = 1; i <= laufzeitInJahre; i++) {
		std::cout << "Betrag nach " << i << " Jahren: " << berechneEndbetrag(anfangskapital, zinssatz, i) << std::endl;
	}
}
